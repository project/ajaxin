<?php

/**
 * @file
 * Hooks and preprocess functions for the Slick module.
 */

use Drupal\Core\Template\Attribute;

/**
 * Prepares variables for ajaxin.html.twig templates.
 */
function template_preprocess_ajaxin(&$variables) {
  $config = ajaxin()->configMultiple('ajaxin.settings');
  foreach (['attributes', 'content_attributes', 'color', 'fs', 'skin'] as $key) {
    $default = $config[$key] ?? '';
    $default = in_array($key, ['attributes', 'content_attributes']) ? [] : $default;
    $variables[$key] = empty($variables['element']["#$key"]) ? $default : $variables['element']["#$key"];
  }

  $items = [];
  $skin = $variables['skin'];
  $count = ajaxin()->getSkins()[$skin] ?? 0;
  $range = range(1, $count);
  if ($count > 0) {
    foreach (array_combine($range, $range) as $delta => $number) {
      $item['attributes'] = new Attribute(['class' => ['sk', 'sk' . $delta]]);
      $items[] = $item;
    }
  }

  $trans = ['pulse', 'rotating-plane', 'wordpress'];
  $variables['is_bg'] = !empty($variables['color']) && in_array($skin, $trans);
  $variables['items'] = $items;
  $variables['config'] = $config;
  $variables['#attached']['library'][] = 'ajaxin/' . $skin;
}
