
# ABOUT AJAXIN

Provides a simple modern ajax progressbar as a replacement for full screen
progressbar, and few AJAX loading animations. Integrated with core AJAX,
Blazy images, and or Intersection Observer ajaxified blocks and its infinite
scroll Views pager.

***
## <a name="first"> </a>FIRST THINGS FIRST!
Read more at:
* [Github](https://git.drupalcode.org/project/blazy/-/blob/3.0.x/docs/README.md#first-things-first)
* [Blazy UI](/admin/help/blazy_ui#first)

## REQUIREMENTS
* [Blazy 3.x](https://www.drupal.org/project/blazy)


## INSTALLATION
Install the module as usual, more info can be found on:

[Installing Drupal 8 Modules](https://drupal.org/node/1897420)


## USAGE / CONFIGURATION
Visit **/admin/config/media/ajaxin**

Change the background color as needed.
The module will override loading animations for core AJAX, Blazy images, and or
Intersection Observer ajaxified blocks and its infinite scroll Views pager.


## KNOWN ISSUES/ LIMITATIONS
* This module doesn't monitor/ calculate AJAX package, yet. It is only a simple
  CSS preloader indicating a progress happening. At this stage, accuracy is not
  crucial. Impression of something in progress is. At best, it just reacts
  on an event when an AJAX or image has finished loading.


# AUTHOR/MAINTAINER/CREDITS
* [Gaus Surahman](https://www.drupal.org/user/159062)
* [Contributors](https://www.drupal.org/node/3048388/committers)
* CHANGELOG.txt for helpful souls with their patches, suggestions and reports.
* [SpinKit at Github](https://github.com/tobiasahlin/SpinKit)
* [SpinKit website](https://tobiasahlin.com/spinkit/)

## READ MORE
See the project page on drupal.org for more updated info:

[Ajaxin module](https://drupal.org/project/ajaxin)
