/**
 * @file
  * Provides Ajaxin functionality to replace Blazy loading animation.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  var _blazy = Drupal.blazy || {};
  var _id = 'blazy';
  var _idOnce = 'b-ajaxin';
  var _mounted = 'is-ajaxin';
  var _gallery = '.' + _id + ':not(.blazy--nojs):not(.' + _mounted + ')';

  /**
   * Utility functions.
   *
   * @param {HTMLElement} elm
   *   The HTML element.
   */
  function process(elm) {
    var init = _blazy.init || null;
    var opts = _blazy.options || _blazy.globals() || {};

    // Merge existing options, including those of bLazy and Bio libraries.
    if (init && init.options) {
      opts = $.extend({}, init.options, opts);
    }

    // Adds the loading animation during page load.
    opts.addNow = true;
    opts.oldLoadingClass = 'is-b-loading';

    // Provides event which should trigger the removal of the preloader.
    // Ajaxin needs to know when to remove its own loading animation.
    opts.doneListener = 'blazy:done';
    opts.root = elm;

    // Initializes the Ajaxin.
    new Ajaxin(opts);
    $.addClass(elm, _mounted);
  }

  /**
   * Attaches blazy behavior to HTML element identified by [data-blazy].
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.ajaxinBlazy = {
    attach: function (context) {
      $.once(process, _idOnce, _gallery, context);
    },
    detach: function (context, setting, trigger) {
      if (trigger === 'unload') {
        $.once.removeSafely(_idOnce, _gallery, context);
      }
    }
  };

}(dBlazy, Drupal, drupalSettings));
