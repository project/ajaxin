/**
 * @file
 * Provides Ajaxin functionality for Drupal AJAX.
 */

(function ($, Drupal, drupalSettings, document) {

  'use strict';

  var _ajax = Drupal.Ajax || {};
  var _proto = _ajax.prototype;
  var _doc = document.documentElement || document.body;
  var _isAjaxin = 'is-ajaxin';
  var _isDone = _isAjaxin + '--done';
  var _wrapper = 'ajaxin-wrapper--fs';
  var _progress = 'ajax-progress';
  var _ajaxTimer;

  /**
   * Overrides Drupal.Ajax.prototype.success event.
   */
  _proto.success = (function (_ajax) {
    return function (response, status) {
      var me = this;
      var el;
      var cn;
      var prog = me.progress;

      if (prog && prog.element && prog.type === 'fullscreen') {
        el = $.toElm(prog.element);

        if ($.isElm(el) && $.hasClass(el, _wrapper)) {
          // Impresses a graceful completion as otherwise abrupt.
          document.body.insertAdjacentHTML('beforeend', Drupal.theme('ajaxProgressIndicatorFullscreen'));

          // Cleans up the progressbar.
          $.addClass(_doc, _isDone);

          clearTimeout(_ajaxTimer);
          _ajaxTimer = setTimeout(function () {
            $.removeClass(_doc, _isAjaxin);
            $.removeClass(_doc, _isDone);
            cn = $.find(_doc, '.' + _wrapper);

            $.remove(cn);
          }, 800);
        }
      }

      return _ajax.apply(this, arguments);
    };
  })(_proto.success);

  /**
   * Overrides Drupal.Ajax.prototype.setProgressIndicatorFullscreen.
   */
  var _fs = _proto.setProgressIndicatorFullscreen;
  _proto.setProgressIndicatorFullscreen = function () {
    $.addClass(_doc, _isAjaxin);
    _fs.call(this);
  };

  // Overrides Drupal.theme.ajaxProgressIndicatorFullscreen().
  Drupal.theme.ajaxProgressIndicatorFullscreen = function () {
    var settings = drupalSettings.ajaxin || {};
    settings.fs = true;
    return Drupal.theme.ajaxin(settings);
  };

  // Overrides Drupal.theme.ajaxProgressThrobber().
  Drupal.theme.ajaxProgressThrobber = function (message) {
    var messageMarkup = typeof message === 'string' ? Drupal.theme('ajaxProgressMessage', message) : '';
    var settings = drupalSettings.ajaxin || {};
    settings.fs = false;
    var throbber = Drupal.theme.ajaxin(settings);

    return '<div class="' + _progress + ' ' + _progress + '-throbber ' + _progress + '--ajaxin">' + throbber + messageMarkup + '</div>';
  };

})(dBlazy, Drupal, drupalSettings, this.document);
